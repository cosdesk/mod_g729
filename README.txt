Freeswitch的mod_g729模块，仅用于G729编码测试，不用于商业目的。

1. 进入freeswitch 源码根目录
cd /usr/local/src/freeswitch

2. 删除默认g729模块
rm -rf src/mod/codecs/mod_g729

3. 解压并移动当前代码到codecs目录 (解压到当前用户目录 ~）

mv ~/mod_g729 /src/mod/codecs/ 

4. 编译安装
vi modules.conf
取消此行的注释： codecs/mod_g729

make mod_g729-install