BASE=../../../..

SUBDIRS=g729a_v11

	
LIBG729_OBJS=g729a_v11/*.o

LOCAL_OBJS=$(LIBG729_OBJS)
LOCAL_CFLAGS=-Ig729a_v11/

g729a_v11/*.o : subdirs

subdirs :
	for dir in $(SUBDIRS); do \
		$(MAKE) -C $$dir; \
	done

include $(BASE)/build/modmake.rules